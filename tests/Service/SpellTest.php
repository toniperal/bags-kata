<?php

namespace Bags\Tests\Service;

use Bags\App\Exception\AllowedNameException;
use Bags\App\Model\Item;
use Bags\App\Model\Player;
use Bags\App\Service\BackPackService;
use Bags\App\Service\BagService;
use Bags\App\Service\CategoryService;
use Bags\App\Service\ItemService;
use Bags\App\Service\PlayerService;
use Bags\App\Service\SpellService;
use Bags\Tests\Factory\BackPackFactory;
use PHPUnit\Framework\TestCase;

class SpellTest extends TestCase
{
    public const ITEMS_24_NAME = [
        'Leather',
        'Linen',
        'Silk',
        'Wool',
        'Copper',
        'Gold',
        'Iron',
        'Silver',
        'Axe',
        'Dagger',
        'Mace',
        'Sword',
        'Cherry Blossom',
        'Marigold',
        'Rose',
        'Seaweed',
        'Leather',
        'Linen',
        'Silk',
        'Wool',
        'Iron',
        'Iron',
        'Iron',
        'Silver',
    ];

    /** @var SpellService $spellService */
    private $spellService;

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var Player $player */
    private $player;

    /** @var BackPackFactory $backPackFactory */
    private $backPackFactory;

    public function setUp(): void
    {
        $this->backPackFactory = new BackPackFactory();
        $itemService = new ItemService();
        $categoryService = new CategoryService();
        $bagService = new BagService($categoryService);
        $backPackService = new BackPackService($bagService, $itemService);
        $this->playerService = new PlayerService($backPackService);
        $this->spellService = new SpellService($itemService, $categoryService);
    }

    /**
     * @throws AllowedNameException
     */
    public function testOrganizingSpell()
    {
        $this->player = $this->playerService->getPlayer();
        $this->backPackFactory->createBackPackWithItems($this->player->getBackpack(), self::ITEMS_24_NAME);
        $this->spellService->organizingSpell($this->player->getBackpack());
        $this->assertContains(Item::ITEMS[10], $this->player->getBackpack()->getItemsContainer()->getItem());
    }
}