<?php

namespace Bags\Tests\Service;

use Bags\App\Exception\AllowedNameException;
use Bags\App\Exception\MaximumItemsAllowedBackPack;
use Bags\App\Model\Item;
use Bags\App\Model\Player;
use Bags\App\Service\BackPackService;
use Bags\App\Service\BagService;
use Bags\App\Service\CategoryService;
use Bags\App\Service\ItemService;
use Bags\App\Service\PlayerService;
use PHPUnit\Framework\TestCase;

/**
 * Class BackPackTest
 * @package Bags\Tests\Service
 */
class BackPackTest extends TestCase
{
    public const ITEMS_MORE_24_INDEX = [0,1,3,4,5,6,7,8,11,13,9,14,15,10,1,2,3,3,3,3,3,3,3,3,6];

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var BackPackService $backPackService */
    private $backPackService;

    /** @var Player $player */
    private $player;

    public function setUp(): void
    {
        $categoryService = new CategoryService();
        $bagService = new BagService($categoryService);
        $itemService = new ItemService();
        $this->backPackService = new BackPackService($bagService, $itemService);
        $this->playerService = new PlayerService($this->backPackService);
    }

    /**
     * @throws AllowedNameException
     */
    public function testAddItemsToBackPackMore24()
    {
        try {
            $this->player = $this->playerService->getPlayer();
            foreach(self::ITEMS_MORE_24_INDEX as $itemIndex){
                $this->backPackService->addItemToBackPack($this->player->getBackpack(), Item::ITEMS[$itemIndex]);
            }
            $this->expectException(MaximumItemsAllowedBackPack::class);

        } catch (MaximumItemsAllowedBackPack $e) {
            $this->assertStringContainsString('The number of items allowed are 24', $e->getMessage());
        }
    }

    /**
     * @throws MaximumItemsAllowedBackPack
     * @throws AllowedNameException
     */
    public function testAddItemsToBackPack()
    {
        $this->player = $this->playerService->getPlayer();
        $count = 0;
        while ($count <= 15) {
            $this->backPackService->addItemToBackPack($this->player->getBackpack(), Item::ITEMS[$count]);
            $count++;
        }
        $this->assertContains(Item::ITEMS[0], $this->player->getBackpack()->getItemsContainer()->getItem());
    }
}