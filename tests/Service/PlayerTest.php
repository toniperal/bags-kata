<?php

namespace Bags\Tests\Service;

use Bags\App\Model\Player;
use Bags\App\Service\BackPackService;
use Bags\App\Service\BagService;
use Bags\App\Service\CategoryService;
use Bags\App\Service\ItemService;
use Bags\App\Service\PlayerService;
use PHPUnit\Framework\TestCase;

/**
 * Class PlayerTest
 * @package Bags\App\Tests\Service
 */
class PlayerTest extends TestCase
{
    /** @var PlayerService $playerService */
    private $playerService;

    public function setUp(): void
    {
        $itemService = new ItemService();
        $categoryService = new CategoryService();
        $bagService = new BagService($categoryService);
        $backPackService = new BackPackService($bagService, $itemService);
        $this->playerService = new PlayerService($backPackService);
    }

    public function testInitializerPlayerWithBackPack()
    {
        $player = $this->playerService->getPlayer();
        $this->assertInstanceOf(Player::class,$player);
    }
}