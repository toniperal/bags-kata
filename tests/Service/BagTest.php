<?php

namespace Bags\Tests\Service;

use Bags\App\Model\Item;
use Bags\App\Service\BackPackService;
use Bags\App\Service\BagService;
use Bags\App\Service\CategoryService;
use Bags\App\Service\ItemService;
use Bags\App\Service\PlayerService;
use PHPUnit\Framework\TestCase;

/**
 * Class BagTest
 * @package Bags\Tests\Service
 */
class BagTest extends TestCase
{
    /** @var BagService $bagService */
    private $bagService;

    /** @var PlayerService $player */
    private $playerService;

    public function setUp(): void
    {
        $itemService = new ItemService();
        $categoryService = new CategoryService();
        $this->bagService = new BagService($categoryService);
        $backPackService = new BackPackService($this->bagService, $itemService);
        $this->playerService = new PlayerService($backPackService);
    }

    public function testCreateBag(){
        $player = $this->playerService->initializerPlayerWithBackPack();
        $this->bagService->createBags($player->getBackpack());
        $this->assertCount(4,$player->getBackpack()->getBag());
    }
}