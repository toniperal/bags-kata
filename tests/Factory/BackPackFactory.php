<?php

namespace Bags\Tests\Factory;

use Bags\App\Exception\AllowedNameException;
use Bags\App\Model\Backpack;
use Bags\App\Model\Bag;
use Bags\App\Model\Item;
use Bags\App\Service\BackPackService;
use Bags\App\Service\BagService;
use Bags\App\Service\CategoryService;
use Bags\App\Service\ItemService;
use Bags\App\Service\PlayerService;
use Bags\App\Service\SpellService;

class BackPackFactory
{
    /** @var SpellService $spellService */
    private $spellService;

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var BagService $bagService */
    private $bagService;

    public function __construct()
    {
        $itemService = new ItemService();
        $categoryService = new CategoryService();
        $this->bagService = new BagService($categoryService);
        $backPackService = new BackPackService($this->bagService, $itemService);
        $this->playerService = new PlayerService($backPackService);
        $this->spellService = new SpellService($itemService, $categoryService);

    }

    /**
     * @param Backpack $backPack
     * @param array $items
     * @return Backpack
     * @throws AllowedNameException
     */
    public function createBackPackWithItems(BackPack $backPack, array $items): BackPack
    {
        $this->bagService->createBags($backPack);
        foreach($backPack->getBag() as $bag){
            switch ($bag->getCategory()->getName()){
                case 'Clothes':
                    $bag->setItem(Bag::ALLOWED_CATEGORIES_AND_ITEMS['Clothes']);
                    break;
                case 'Metals':
                    $bag->setItem(Bag::ALLOWED_CATEGORIES_AND_ITEMS['Metals']);
                    break;
                case 'Weapons':
                    $bag->setItem(Bag::ALLOWED_CATEGORIES_AND_ITEMS['Weapons']);
                    break;
                case 'Herbs':
                    $bag->setItem(Bag::ALLOWED_CATEGORIES_AND_ITEMS['Herbs']);
                    break;
            }
        }
        $item = new Item();
        $item->setItem([
            'Leather',
            'Silk',
            'Wool',
            'Copper',
            'Gold',
            'Iron',
            'Axe',
            'Dagger'
        ]);

        $backPack->setItemsContainer($item);
        return $backPack;
    }
}