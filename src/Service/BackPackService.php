<?php

namespace Bags\App\Service;

use Bags\App\Exception\AllowedNameException;
use Bags\App\Exception\MaximumItemsAllowedBackPack;
use Bags\App\Model\Backpack;
use Bags\App\Model\Item;

/**
 * Class BackPackService
 * @package Bags\App\Service
 */
class BackPackService
{
    /** @var BagService $bagService */
    private $bagService;

    /** @var ItemService $itemService */
    private $itemService;

    public function __construct(BagService $bagService, ItemService $itemService){
        $this->bagService = $bagService;
        $this->itemService = $itemService;
    }

    /**
     * @return Backpack
     */
    public function createBackPack(): Backpack
    {
        $backPack = new Backpack();
        $this->bagService->createBags($backPack);

        return $backPack;
    }

    /**
     * @param Backpack $backPack
     * @param string $nameItem
     * @return Backpack
     * @throws MaximumItemsAllowedBackPack|AllowedNameException
     */
    public function addItemToBackPack(BackPack $backPack, string $nameItem): Backpack
    {
        try {
            $this->checkNumberItemsTotal($backPack);
            if ($this->checkNumberItemsInBackPack($backPack)) {
                return $this->addItemToMainBackPack($backPack, $nameItem);
            } else {
                return $this->bagService->addItemToBag($backPack, $nameItem);
            }
        } catch (MaximumItemsAllowedBackPack $e) {
            throw $e;
        }
    }

    /**
     * @param Backpack $backPack
     * @param string $nameItem
     * @return Backpack
     * @throws AllowedNameException
     */
    private function addItemToMainBackPack(BackPack $backPack, string $nameItem): Backpack
    {
        $items = [];
        if (!empty($backPack->getItemsContainer())) {
            foreach ($backPack->getItemsContainer()->getItem() as $item) {
                $items[] = $item;
            }
        }
        array_push($items, $nameItem);
        $item = new Item();
        $item->setItem($items);
        $backPack->setItemsContainer($item);

        return $backPack;
    }

    /**
     * @param Backpack $backPack
     * @return bool
     */
    public function checkNumberItemsInBackPack(BackPack $backPack): bool
    {
        if (!empty($backPack->getItemsContainer()) &&
            count($backPack->getItemsContainer()->getItem()) >= Backpack::MAX_ALLOWED_ITEMS_BACKPACK) {
            return false;
        }

        return true;
    }

    /**
     * @param Backpack $backPack
     * @throws MaximumItemsAllowedBackPack
     */
    public function checkNumberItemsTotal(BackPack $backPack)
    {
        $allItems = $this->itemService->getAllItems($backPack);
        if (!empty($allItems) &&
            count($allItems) >= Backpack::MAX_ALLOWED_ITEMS) {
            throw new MaximumItemsAllowedBackPack();
        }
    }
}