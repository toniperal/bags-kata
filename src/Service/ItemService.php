<?php


namespace Bags\App\Service;


use Bags\App\Exception\AllowedNameException;
use Bags\App\Model\Backpack;

class ItemService
{
    /**
     * @param Backpack $backPack
     * @return array
     */
    public function getAllItems(BackPack $backPack): array
    {
        $items = [];

        if (!empty($backPack->getItemsContainer())) {
            foreach ($backPack->getItemsContainer()->getItem() as $item) {

                $items[] = $item;
            }

        }
        if (!empty($backPack->getBag())) {
            foreach ($backPack->getBag() as $bag) {
                $itemsContainer = $bag->getItem() ?? [];
                foreach ($itemsContainer as $item) {
                    $items[] = $item;
                }
            }
        }

        return $items;
    }

    /**
     * @param Backpack $backPack
     * @return Backpack
     * @throws AllowedNameException
     */
    public function cleanContainers(BackPack $backPack)
    {
        $backPack->getItemsContainer()->setItem([]);

        if (!empty($backPack->getBag())) {
            foreach ($backPack->getBag() as $bag) {
                $bag->setItem(null);
            }
        }
        return $backPack;
    }
}