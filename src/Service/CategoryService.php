<?php

namespace Bags\App\Service;

use Bags\App\Model\Bag;

/**
 * Class CategoryService
 * @package Bags\App\Service
 */
class CategoryService
{
    /**
     * @param string $nameItem
     * @return int|string
     */
    public function getCategoryNameByBItem(string $nameItem)
    {
        foreach (Bag::ALLOWED_CATEGORIES_AND_ITEMS as $key => $value) {
            $search = array_search($nameItem, $value);
            if ($search === 0 || $search) {
                return $key;
            }
        }
    }
}