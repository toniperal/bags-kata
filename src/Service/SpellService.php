<?php

namespace Bags\App\Service;

use Bags\App\Exception\AllowedNameException;
use Bags\App\Model\Backpack;
use Bags\App\Model\Bag;
use Bags\App\Model\Item;

class SpellService
{
    /** @var ItemService $itemService */
    private $itemService;

    /** @var CategoryService $categoryService */
    private $categoryService;

    public function __construct(ItemService $itemService, CategoryService $categoryService){
        $this->itemService = $itemService;
        $this->categoryService = $categoryService;
    }

    /**
     * @param Backpack $backPack
     * @return Backpack
     * @throws AllowedNameException
     */
    public function organizingSpell(BackPack $backPack): BackPack
    {
        $items = $this->itemService->getAllItems($backPack);
        $backPack = $this->itemService->cleanContainers($backPack);

        return $this->addItemToBagSpell($backPack, $items);
    }

    /**
     * @param Backpack $backPack
     * @param array $allItems
     * @return Backpack
     * @throws AllowedNameException
     */
    private function addItemToBagSpell(BackPack $backPack, array $allItems): Backpack
    {
        foreach ($allItems as $key => $item) {
            $nameCategory = $this->categoryService->getCategoryNameByBItem($item);

            foreach ($backPack->getBag() as $bag) {

                $itemsContainer = $bag->getItem() ?? [];
                if (in_array($item, Bag::ALLOWED_CATEGORIES_AND_ITEMS[$nameCategory])
                    && count($itemsContainer) < Backpack::MAX_ALLOWED_BAG
                    && $nameCategory === $bag->getCategory()->getName()) {

                    $items = $itemsContainer;
                    array_push($items, $item);
                    sort($items);
                    $bag->setItem($items);
                    unset($allItems[$key]);
                }
            }
        }
        $item = new Item();
        $item->setItem($allItems);
        $backPack->setItemsContainer($item);

        return $backPack;
    }
}