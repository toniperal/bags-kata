<?php

namespace Bags\App\Service;

use Bags\App\Model\Player;

/**
 * Class PlayerService
 * @package Bags\App\Service
 */
class PlayerService
{
    /** @var BackPackService $backPackService */
    private $backPackService;

    /** @var Player $player */
    private $player;

    /**
     * PlayerService constructor.
     * @param BackPackService $backPackService
     */
    public function __construct
    (
        BackPackService $backPackService
    )
    {
        $this->backPackService = $backPackService;
        $this->player = $this->initializerPlayerWithBackPack();
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }

    /**
     * @return Player
     */
    public function initializerPlayerWithBackPack(): Player
    {
        $player = new Player();
        $backPack = $this->backPackService->createBackPack();
        $player->setBackpack($backPack);
        return $player;
    }

}