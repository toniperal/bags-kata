<?php

namespace Bags\App\Service;

use Bags\App\Model\Backpack;
use Bags\App\Model\Bag;
use Bags\App\Model\Category;

/**
 * Class BagService
 * @package Bags\App\Service
 */
class BagService
{
    /** @var CategoryService $categoryService */
    private $categoryService;

    /**
     * BagService constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService){
        $this->categoryService = $categoryService;
    }

    /**
     * @param Backpack $backpack
     * @return Backpack
     */
    public function createBags(Backpack $backpack): Backpack
    {
        $bags = [];
        foreach (Bag::ALLOWED_CATEGORIES_AND_ITEMS as $key => $value) {
            $category = new Category();
            $category->setName($key);
            $bag = new Bag();
            $bag->setCategory($category);
            $bags[] = $bag;
        }
        return $backpack->setBag($bags);
    }

    /**
     * @param Backpack $backPack
     * @param string $nameItem
     * @return Backpack
     */
    public function addItemToBag(BackPack $backPack, string $nameItem): Backpack
    {
        foreach ($backPack->getBag() as $bag) {

            $itemsContainer = $bag->getItem() ?? [];
            if (count($itemsContainer) < Backpack::MAX_ALLOWED_BAG) {
                $items = $itemsContainer;
                array_push($items, $nameItem);
                $bag->setItem($items);
                break;
            }
        }
        return $backPack;
    }
}