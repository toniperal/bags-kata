<?php

namespace Bags\App\Model;

/**
 * Class Bag
 * @package Bags\App\Model
 */
class Bag
{
    public const ALLOWED_CATEGORIES_AND_ITEMS = [
        'Clothes'=>['Leather','Linen','Silk','Wool'],
        'Metals'=>['Copper','Gold','Iron','Silver'],
        'Weapons'=>['Axe','Dagger','Mace','Sword'],
        'Herbs'=>['Cherry Blossom','Marigold','Rose','Seaweed']
    ];

    /** @var Item[]|null */
    private $item;

    /** @var Category|null */
    private $category;

    /**
     * @return Item[]|null
     */
    public function getItem(): ?array
    {
        return $this->item;
    }

    /**
     * @param Item[]|null $item
     * @return Bag
     */
    public function setItem(?array $item): Bag
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return Bag
     */
    public function setCategory(?Category $category): Bag
    {
        $this->category = $category;
        return $this;
    }
}