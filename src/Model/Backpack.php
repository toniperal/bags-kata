<?php

namespace Bags\App\Model;

/**
 * Class Backpack
 * @package Bags\App\Model
 */
class Backpack
{
    public const MAX_ALLOWED_ITEMS_BACKPACK = 8;
    public const MAX_ALLOWED_BAG = 4;
    public const MAX_ALLOWED_ITEMS = 24;

    /** @var Bag[]|null */
    private $bag;

    /** @var Item|null */
    private $itemsContainer;

    /**
     * @return Bag[]|null
     */
    public function getBag(): ?array
    {
        return $this->bag;
    }

    /**
     * @param Bag[]|null $bag
     * @return Backpack
     */
    public function setBag(?array $bag): Backpack
    {
        $this->bag = $bag;
        return $this;
    }

    /**
     * @return Item|null
     */
    public function getItemsContainer(): ?Item
    {
        return $this->itemsContainer;
    }

    /**
     * @param Item|null $itemsContainer
     * @return Backpack
     */
    public function setItemsContainer(?Item $itemsContainer): Backpack
    {
        $this->itemsContainer = $itemsContainer;
        return $this;
    }
}