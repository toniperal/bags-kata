<?php

namespace Bags\App\Model;

use Bags\App\Exception\AllowedNameException;

/**
 * Class Item
 * @package Bags\App\Model
 */
class Item
{
    public const ITEMS = [
        'Leather',
        'Linen',
        'Silk',
        'Wool',
        'Copper',
        'Gold',
        'Iron',
        'Silver',
        'Axe',
        'Dagger',
        'Mace',
        'Sword',
        'Cherry Blossom',
        'Marigold',
        'Rose',
        'Seaweed',
    ];

    /** @var array $item */
    private $item;

    /**
     * @return array
     */
    public function getItem(): array
    {
        return $this->item;
    }

    /**
     * @param array $item
     * @return Item
     * @throws AllowedNameException
     */
    public function setItem(array $item): Item
    {
        try{
            if($this->allowedItem($item)){
                $this->item = $item;
                return $this;
            }
        }catch (AllowedNameException $e){
            throw $e;
        }
    }

    /**
     * @param array $items
     * @return bool
     * @throws AllowedNameException
     */
    public function allowedItem(array $items){

        foreach($items as $item){
            if(!in_array($item,self::ITEMS,true)){
                throw new AllowedNameException($item);
            }
        }
        return true;
    }
}