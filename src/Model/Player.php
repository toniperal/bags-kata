<?php

namespace Bags\App\Model;

/**
 * Class Player
 * @package Bags\App\Model
 */
class Player
{

    /** @var Backpack $backpack */
    private $backpack;

    /**
     * @return Backpack
     */
    public function getBackpack(): Backpack
    {
        return $this->backpack;
    }

    /**
     * @param Backpack $backpack
     * @return Player
     */
    public function setBackpack(Backpack $backpack): Player
    {
        $this->backpack = $backpack;
        return $this;
    }

}