<?php

namespace Bags\App\Model;

/**
 * Class Category
 * @package Bags\App\Model
 */
class Category
{
    public const CATEGORIES = ['Clothes','Metals','Weapons','Herbs'];

    /** @var string $name */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        if($this->allowedCategory($name)){
            return $this;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function allowedCategory(string $name){

        if(in_array($name,self::CATEGORIES,true)){
            return true;
        }
        return false;
    }
}