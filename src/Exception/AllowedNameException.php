<?php
namespace Bags\App\Exception;

/**
 * Class AllowedNameException
 * @package Bags\App\Exception
 */
class AllowedNameException extends \Exception
{
    public const NUMBER_ITEM_ALLOWED = 'item name not allowed: ';

    /**
     * AllowedNameException constructor.
     * @param null $field
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($field = NULL, $message="", $code=0 , \Exception $previous=NULL)
    {
        if(empty($message)){
            $message = self::NUMBER_ITEM_ALLOWED. $field;
        }
        parent::__construct($message, $code, $previous);
    }
}