<?php
namespace Bags\App\Exception;

use Bags\App\Model\Backpack;
use Throwable;

/**
 * Class MaximumItemsAllowedBackPack
 * @package Bags\App\Exception
 */
class MaximumItemsAllowedBackPack extends \Exception
{
    public const NUMBER_ITEM_ALLOWED = 'The number of items allowed are ';

    /**
     * MaximumItemsAllowedBackPack constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if(empty($message)){
            $message = self::NUMBER_ITEM_ALLOWED. Backpack::MAX_ALLOWED_ITEMS;
        }

        parent::__construct($message, $code, $previous);

    }
}